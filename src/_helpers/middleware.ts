export const Authenticated = (req, res, next) => {
    if (!req.isAuthenticated()) {
        return res.status(403).json({ error: 'Not authorized' })
    }
    next()
}

export const Cors = (req, res, next) => {
    res.header('Access-Control-Allow-Credentials', true)
    res.header(
        'Access-Control-Allow-Origin',
        process.env.FRONTEND_PROTOCOL + '://' + process.env.FRONTEND_DOMAIN.toLowerCase(),
    )
    res.header('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE')
    res.header(
        'Access-Control-Allow-Headers',
        'Authorization, Origin, X-Requested-With, Content-Type, Accept',
    )

    if ('OPTIONS' === req.method) {
        res.sendStatus(200)
    } else {
        next()
    }
}
