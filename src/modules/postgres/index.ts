import { Sequelize } from 'sequelize'

const sequelize = new Sequelize(
    process.env.DATABASE,
    process.env.DATABASE_USERNAME,
    process.env.DATABASE_PASSWORD,
    {
        host: `${process.env.DATABASE_HOST}` || 'localhost',
        dialect: 'postgres',
        pool: {
            max: 16,
            min: 0,
            acquire: 30000,
            idle: 10000,
        },
        logging: false,
    },
)

export class Postgres {
    init() {
        new Sequelize(process.env.DATABASE, process.env.DATABASE_USERNAME, process.env.DATABASE_PASSWORD, {
            host: `${process.env.DATABASE_HOST}` || 'localhost',
            dialect: 'postgres',
            pool: {
                max: 16,
                min: 0,
                acquire: 30000,
                idle: 10000,
            },
            logging: false,
        })
    }
}

export const dbInstance: Sequelize = sequelize
