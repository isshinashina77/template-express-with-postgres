import * as express from 'express'

export = (() => {
    let router = express.Router()

    router.get('', (req, res) => {
        res.json({ online: true })
    })

    return router
})()
