require('dotenv').config()

import { Server } from './server'
import { Postgres } from './modules/postgres'

const server = new Server()

new Postgres().init()
server.mountRoutes()

server.listen((e, port) => {
    if (e) {
        console.error('Could not start server')
        process.exit(e)
    }
    console.log(`Listening on port ${port}`)
})

async function exitHandler(options, exitCode) {
    if (options.cleanup) {
        console.log('clean')
    }
    if (exitCode || exitCode === 0) {
        console.log(`Exitting with exit code`, exitCode)
    }
    server.shutdown()
    // Sleep for a second to avoid the EADDRINUSE error
    await new Promise<void>((r) => {
        setTimeout(() => {
            r()
        }, 1000)
    })
    if (options.exit) process.exit()
}

// process.stdin.resume();
process.on('exit', exitHandler.bind(null, { cleanup: true }))
// process.on('SIGINT', exitHandler.bind(null, { exit: true }))
// process.on('SIGUSR1', exitHandler.bind(null, { exit: true }))
// process.on('SIGUSR2', exitHandler.bind(null, { exit: true }))
process.on('uncaughtException', exitHandler.bind(null, { exit: true }))
