FROM node:alpine

WORKDIR /src

COPY package.json ./
COPY yarn.lock ./
RUN yarn install

COPY . .
RUN yarn run build

WORKDIR /src/dist

EXPOSE 8000
CMD node index.js
